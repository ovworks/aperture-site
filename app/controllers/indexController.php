<?php
    class indexController extends baseController{
        public function indexAction(){
            echo $this->view("index/index.html");
        }

        public function notFoundAction(){
            echo $this->view("index/404.html");
        }
    }
?>
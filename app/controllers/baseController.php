<?php
    class baseController extends apertureController{
        public function __construct(){
            parent::__construct();
            
            date_default_timezone_set('Pacific/Auckland');
        }
        
        public function auth(){
            
        }
        
        public function gateKeep($inverse = false){
            return true;
        }
        
        public function _extendViewVars($vars){
            $vars['yourGlobalVar'] = true;
            
            return $vars;
        }
    }
?>
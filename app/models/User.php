<?php
    class User extends apertureModel{
        public $passwordAgain;
        public $validatePassword = true;
        
        static $salt = "\$up_\$mi77y2\&";
        
        static $validates_presence_of = array(
            array('firstname'),
            array('lastname'),
            array('email')
        );
        
        static $validates_size_of = array(
            array('password', 'within' => array(6, 12))
        );
        
        public function validate(){

            if($this->validatePassword){
                if($this->password != $this->passwordAgain){
                    $this->errors->add('passwordAgain', 'Passwords do not match');
                }
            }
            
            if(isset($this->id)){
                if(User::exists(array("conditions" => array("id <> ? AND email = ?", $this->id, $this->email)))){
                    $this->errors->add('email', 'Exists');
                }
            }else{
                if(User::exists(array("conditions" => array("email = ?", $this->email)))){
                    $this->errors->add('email', 'Exists');
                }
            }
        }
        
        public static function authenticate($email, $password){
            $user = User::find_by_email($email);
            
            if($user){
                if(!$user->is_deleted){
                    if(User::matchPassword($password, $user->password)){
                        return $user;
                    }else{
                        return false;
                    }
                }else{
                    return false;
                }
            }else{
                return false;
            }
        }
        
        public static function get($id){
            if(is_numeric($id)){
                $user = User::find("first", array("conditions" => array("id = ? AND is_deleted = ?", $id, 0)));
            }else{
                $user = User::find("first", array("conditions" => array("email = ? AND is_deleted = ?", $id, 0)));
            }
            
            if($user){
                return $user;
            }else{
                return false;
            }
        }
        
        public static function getAll($admin = false){
            if($admin){
                $users = User::all(array("conditions" => array("is_admin = ? AND is_deleted = ?", 1, 0)));
            }else{
                $users = User::all(array("conditions" => array("is_deleted = ?", 0)));
            }
            
            return $users;
        }
        
        public static function createNew($firstname, $lastname, $email, $password, $passwordAgain, $is_admin = 0){
            
            $user = new User();
            
            $user->firstname = $firstname;
            $user->lastname = $lastname;
            $user->email = $email;
            $user->password = $password;
            $user->passwordAgain = $passwordAgain;
            $user->is_admin = $is_admin;
            
            if($user->is_valid()){
                $user->password = User::password($password);
                $user->save(false);
            }
            
            return $user;
        }
        
        public static function change($id, $firstname, $lastname, $email, $password, $passwordAgain, $is_admin = 0){
            
            $user = User::get($id);
            
            if($user){
                $user->firstname = $firstname;
                $user->lastname = $lastname;
                $user->email = $email;
                
                if(trim($password) != ""){
                    $user->password = $password;
                    $user->passwordAgain = $passwordAgain;
                }else{
                    $user->validatePassword = false;
                    $currentPassword = $user->password;
                    $user->password = "temporary";
                }
                $user->is_admin = $is_admin;
                
                if($user->is_valid()){
                    if($user->validatePassword){
                        $user->hashPassword();
                        $user->save(false);
                    }else{
                        unset($user->password);
                        $user->password = $currentPassword;
                        $user->save(false);
                    }
                }
            }
            
            return $user;
        }
        
        /* FUTURE USE BCRYPT FOR SECURITY
        
        public static function password($password){
            $hash = password_hash($password, PASSWORD_BCRYPT, array("cost" => 10));
            return $hash;
        }
        
        public static function matchPassword($password, $passwordHash){
            return password_verify($password, $passwordHash);
        }
        
        */
        
        public static function password($password){
            $password = User::$salt . $password;
            $hash = hash("sha256", $password);
            return $hash;
        }
        
        public function hashPassword(){
            $password = User::$salt . $this->password;
            $this->password = hash("sha256", $password);
            return $this->password;
        }
        
        public static function matchPassword($password, $passwordHash){
            return (User::password($password) == $passwordHash ? true : false);
        }
        
    }
?>
<?php
class Routes extends apertureRoutes{
    public function route(){
        $app = $this->app;
        
        $this->loadController("base");
        
        // General Routes
        $app->route('GET /', $this->to("index", "index"));
        $app->route('GET /index', $this->to("index", "index"));
        
        // System Routes
        $app->map('notFound', $this->to("index","notFound"));
        
        $app->start();
    }
}
?>
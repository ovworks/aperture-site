$.fn.exists = function() {
    return this.length > 0;
}

$.fn.hasAttr = function(name) {  
   var attr = this.attr(name);
   return typeof attr !== 'undefined' && attr !== false;
};

function main(type) {

    this.init = function() {
        $(document).foundation();
    }

}

var main = new main();

$(document).on("ready", function() {
    main.init();
});